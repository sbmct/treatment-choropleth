import dbf
import random

db = dbf.Table('DMWWGS84/DMAWGS84.dbf')
with db:
    db.add_fields('data N(12,7)')
    for record in db:
        dbf.write(record, data=random.random())    ## replace random.random with whatever you want to insert for that row
